import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import { Http, Headers } from '@angular/http';
import { HttpRequest } from '@angular/common/http';
import { ROOT_URL } from '../shared/shared';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { IUser } from './user.model';
import { MessageService } from '../messages/message.service';
import * as sha256 from "sha256";


//import * as sha256 from "fast-sha256";

@Injectable()
export class AuthService {

    currentUser: IUser ;
    redirectUrl: string;

    constructor(private http: Http, private cookie: CookieService, private messageService: MessageService) {


    }

    getAccessToken(): string {
        return this.cookie.get('access_token');
    }

    refreshToken(): Observable<any> |  any {
        let headers = new Headers();
        headers.set('Content-Type', 'multipart/form-data');
        let params = "grant_type=refresh_token&refresh_token=" + this.cookie.get('refresh_token') + "&client_id=ismail&client_secret=ismail";
        return this.http.post(ROOT_URL + "oauth/token?" + params, { headers: headers })
            .map(data => {
                let tokenExpireDate = new Date().getTime() + (1000 * data.json().expires_in);
                this.cookie.set('access_token', data.json().access_token, tokenExpireDate);
                console.log("refresh sent");

                return data.json();

            });

    }

    login(email: string, password: string): Observable<any> {
        //64da817f607e940546c59f63e69dc0cf1e2e4b079de5b083cd6e1800f669b87c
           
            this.cookie.deleteAll();
            let headers = new Headers();
            headers.set('Content-Type', 'multipart/form-data');
            let pass = sha256(password)
            let params1 = "grant_type=password&username=ismail@hf.com&password=64da817f607e940546c59f63e69dc0cf1e2e4b079de5b083cd6e1800f669b87c&client_id=ismail&client_secret=ismail";
            let params = "grant_type=password&username=" + email + "&password=" + pass + "&client_id=ismail&client_secret=ismail";
            console.log('login called');
            return this.http.post(ROOT_URL + "oauth/token?" + params, { headers: headers })
                .map(data => {
                    console.log('request sent');
                    let tokenExpireDate = new Date().getTime() + (1000 * data.json().expires_in);
                    let refreshTokenExpireDate = new Date().getTime() + (1000 * 50000);
                    this.cookie.set('access_token', data.json().access_token, tokenExpireDate);
                    console.log('Cookies sent');
                    this.cookie.set('refresh_token', data.json().refresh_token, refreshTokenExpireDate);
                    this.cookie.set('Id', data.json().user.id, refreshTokenExpireDate);
                    this.currentUser = {
                        id:data.json().user.id,
                        username : data.json().user.username,
                        email : data.json().user.email,
                        isAdmin : true
                    };
                    this.messageService.addMessage(`User: ${this.currentUser.username} logged in`);
                    
                    return data.json();
                });
    }

    signup(user : any){
        let headers = new Headers();
        headers.set('Content-Type', 'application/json');
        console.log(JSON.stringify(user));
        return this.http.post(ROOT_URL + "users",JSON.stringify(user),{ headers: headers });
    }

    isLoggedIn(): boolean {
        return this.cookie.check('refresh_token');
    }

    getUserId(): string{
        return this.cookie.get('Id');
    }

    /*login(userName: string, password: string): void {
        if (!userName || !password) {
            this.messageService.addMessage('Please enter your userName and password');
            return;
        }
        this.messageService.addMessage(`User: ${this.currentUser.username} logged in`);
    }*/

    logout(): void {
        this.cookie.deleteAll();
        this.currentUser = null;
        console.log("auth service logout");
    }
}