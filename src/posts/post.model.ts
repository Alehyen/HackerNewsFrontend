import { IComment } from './comment.model';

export interface IPost {
    
    id? : string;
    title : string;
    body : string;
    createdAt : Date;
    author : string;
    comments? : Array<IComment>
   
   }