import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { IPost } from './post.model';
import { PostService } from './post.service';

@Injectable()
export class PostResolver implements Resolve<IPost[]> {

    constructor(private postService: PostService,
                private router: Router) { }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<IPost[]> {
        return this.postService.getPosts().map(data=>{
            return data;
        },
    err=>{
        console.log("no posts was found! try adding a Post ;)");
    })
            
    }
}
