import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CookieService } from 'ngx-cookie-service';
//import { InMemoryWebApiModule } from 'angular-in-memory-web-api';


import { UserModule } from '../user/user.module';
import { MessageModule } from '../messages/message.module';

import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from '../home/home.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { AppComponent } from './app.component';
import { RequestInterceptorService } from '../user/request-interceptor.service';
import { AuthService } from "../user/auth.service";


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    UserModule,
    MessageModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true },
    AuthService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
