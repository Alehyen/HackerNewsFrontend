import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPost } from './post.model';



@Component({
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
    pageTitle: string = 'Post List';
    errorMessage: string;
    posts: IPost[];

    constructor(private route: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.route.data.subscribe(data => {
            this.posts = data['posts'];
        });
    }


}
