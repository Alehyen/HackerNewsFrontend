import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import * as sha256 from "sha256";


@Component({
    templateUrl: './signup.component.html'
})
export class SignupComponent {
    errorMessage: string;
    pageTitle = 'Sign up';
    user = {
        email: null,
        password: null
    }

    constructor(private authService: AuthService,
        private router: Router) { }

    signup() {
        if (this.user.email && this.user.password) {
            this.user.password = sha256(this.user.password);
            return this.authService.signup(this.user)
                .subscribe(data => {
                    console.log("signup succesful");
                    this.user = null;
                    this.router.navigate(['/login']);
                });
        } else {
            this.errorMessage = "Both email and password are required!";
        }
    }
}
