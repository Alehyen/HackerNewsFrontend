import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { PostService } from './post.service'
import { IPost } from './post.model';
import { AuthService } from '../user/auth.service'



@Component({
    templateUrl: './post-add.component.html'
})
export class AddPostComponent {
    errorMessage: string;
    pageTitle = 'Add Post';
    private title : string;
    private body : string;
    private post : IPost;

    constructor(private postService: PostService,
                private authService : AuthService,
                private router: Router) { }

        addPost(){
            if(this.title && this.body){
                this.post={
                    title : this.title,
                    body :  this.body,
                    createdAt : new Date(),
                    author : this.authService.getUserId(),
                    comments : []
                }
                console.log(JSON.stringify(this.post));
                this.postService.addPost(this.post)
                    .subscribe(data=>{
                        console.log("post added");
                        this.router.navigate(['/posts']);
                    })
            }else{
                this.errorMessage = "Title and Body are required!";
            }
        }

    
}
