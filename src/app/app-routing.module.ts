import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuard } from '../user/auth-guard.service';


import { HomeComponent } from '../home/home.component';
import { PageNotFoundComponent } from './page-not-found.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent },
            {
                path: 'posts',
                //canActivate: [ AuthGuard ],
                //data: { preload: true },
                loadChildren: '../posts/post.module#PostModule'
            },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', component: PageNotFoundComponent }
        ])
    ],
    providers: [],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
