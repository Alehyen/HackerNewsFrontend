import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { error } from 'selenium-webdriver';

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent {
    errorMessage: string;
    pageTitle = 'Log In';

    constructor(private authService: AuthService,
                private router: Router) { }

    login(loginForm: NgForm) {
        if (loginForm && loginForm.valid) {
            let username = loginForm.form.value.username;
            let password = loginForm.form.value.password;
            this.authService.login(username, password).subscribe(data=>{
                console.log("logged in");
                if (this.authService.redirectUrl) {
                    // I wait to set cookies before I redirect because if the cookies are not set and I try to redirect the Auth guard redirect
                    // me to login page
                    setTimeout(()=>{
                        this.router.navigateByUrl(this.authService.redirectUrl);
                    },50);
                    
                } else {
                    this.router.navigate(['/posts']);
                }
            },error =>{
                this.errorMessage = "Wrong email/password";
            });

            
        } else {
            this.errorMessage = 'Please enter a user name and password.';
        };
    }
}
