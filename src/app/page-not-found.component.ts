import { Component } from '@angular/core';

@Component({
    template: `
    <h1>Yo! you are in the wrong place.</h1>
    `
})
export class PageNotFoundComponent { }
