# HackerNewsFrontend

 - npm install and Enjoy!

 # Description

- L'application est une copie de site web Hacker News.
- Un utilisateur peut s'inscrire/se connecter pour qu'il peut ajouter des posts ou des commentaires.
- Seul le propriétaire du post a le droit de le supprimer.
- Tout utilisateur peut ajouter des commentaires
- Pour le signin/signup on a utilisé le protocole OAUTH2.
- L'utilisateur doit imperativement se connecter pour qu'il peut ajouter un Post ou voir la liste des Posts.

