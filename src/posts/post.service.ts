import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

import { IPost } from './post.model';
import { ROOT_URL } from '../shared/shared';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Injectable()
export class PostService {


    constructor(private http: HttpClient) {

    }

    getPosts(): Observable<IPost[]> {
        let headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return this.http.get(ROOT_URL + "posts", { headers: headers })
            .map(data => {
                return data as IPost[];
            },
            err => {
                this.handleError(err);
            });
    }

    addPost(post : any){
        let headers = new HttpHeaders();
        headers.set('Content-Type', 'application/json');
        return this.http.post(ROOT_URL + "posts",JSON.stringify(post),{ headers: headers });
        
    }

    private handleError(error: HttpErrorResponse): Observable<any> {
        // in a real world app like monbanquet , we may want to send the error to some remote logging infrastructure
        // instead of just logging it to the console for supervision purposes.
        console.error(error);
        return Observable.throw(error.error || 'Server error');
    }

    
}
