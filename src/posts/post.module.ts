import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostListComponent } from './post-list.component';
import { AddPostComponent } from './post-add.component';
import { PostService } from './post.service';
import { PostResolver } from './post-resolver.service';
import { AuthGuard } from '../user/auth-guard.service';

import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
      SharedModule,
      RouterModule.forChild([
        {
          path: '',
          component: PostListComponent,
          canActivate : [AuthGuard],
          resolve: { posts: PostResolver }
        },
        {
          path: 'add',
          component: AddPostComponent,
          canActivate : [AuthGuard],
        },
      ])
    ],
    declarations: [
        PostListComponent,
        AddPostComponent
    ],
    providers: [
        PostService,
        PostResolver,
        AuthGuard
    ]
  })
  export class PostModule { }
  