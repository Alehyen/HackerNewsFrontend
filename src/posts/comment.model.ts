

export interface IComment {
    
 id : string;
 body : String;
 createdAt : Date;
 writerId : string;

}